from collections import ChainMap
from typing import Any
from typing import Dict

import ruamel
import yaml
from ruamel.yaml.comments import CommentedMap

from idem_codegen.idem_codegen.tool.utils import (
    separator,
)

__contracts__ = ["add_jinja"]


def add_jinja(hub, sls_data: Dict[str, Any], sls_original_data: Dict[str, Any]):
    terraform_resource_map = hub.tf_idem.RUNS["TF_RESOURCE_MAP"]

    # If we do not have terraform_resource_map we cannot parameterize,
    # raise an error if it's not found in hub

    if not terraform_resource_map:
        hub.log.warning(
            "Not able to parameterize, Terraform resource map is not found in hub."
        )
        return sls_data
    sls_data_with_comments = {}
    sls_formatted_data = ruamel.yaml.round_trip_load(
        yaml.dump(sls_data, sort_keys=False)
    )
    for item in sls_data:
        list_of_comments = []
        resource_attributes = list(sls_data[item].values())[0]
        resource_type = list(sls_data[item].keys())[0].replace(".present", "")
        dict(ChainMap(*resource_attributes))

        if not sls_original_data.get(item):
            continue

        original_data_attributes = list(sls_original_data[item].values())[0]
        original_resource_id = dict(ChainMap(*original_data_attributes)).get(
            "resource_id"
        )
        # get terraform resource to look for parameters used
        terraform_resource = terraform_resource_map.get(
            f"{resource_type}{separator}{original_resource_id}"
        )

        if not terraform_resource:
            continue

        list_of_comments.extend(
            hub.tf_idem.exec.generator.jinja.comments.look_for_possible_improvements(
                terraform_resource, resource_attributes
            )
        )
        # sls_file_data_with_comment = ruamel.yaml.round_trip_load(sls_data[item])
        sls_file_data_with_comment = CommentedMap({item: sls_data[item]})
        if len(list_of_comments) > 0:
            sls_formatted_data.yaml_set_comment_before_after_key(
                item, before="\n".join(list_of_comments)
            )
        sls_data_with_comments[item] = sls_file_data_with_comment
    return sls_formatted_data
