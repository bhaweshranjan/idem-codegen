import pytest
import yaml

from tests.test_utils import compare_folders_for_equality


@pytest.mark.asyncio
def test_group_by_resource_type(hub):
    run_name = hub.OPT.idem_codegen.run_name
    sls_data = yaml.safe_load(
        open(f"{hub.test.idem_codegen.current_path}/files/idem_describe_response.sls")
    )

    hub[run_name].RUNS["SLS_DATA_WITH_KEYS"] = sls_data
    output_dir_path = hub.idem_codegen.group.resource_service.segregate(run_name)
    expected_output_dir_path = (
        f"{hub.test.idem_codegen.current_path}/group_resource_type_expected_output"
    )

    compare_folders_for_equality(hub, output_dir_path, expected_output_dir_path)
