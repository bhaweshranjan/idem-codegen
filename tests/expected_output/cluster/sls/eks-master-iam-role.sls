aws_kms_key-credstash_key-key-id-search:
  aws.kms.key.search:
  - resource_id: {{ params.get("aws_kms_key.credstash_key") }}


aws_iam_role.cluster:
  aws.iam.role.present:
  - resource_id: {{ params.get("aws_iam_role.cluster")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster
  - arn: arn:aws:iam::123456789012:role/idem-test-temp-xyz-cluster
  - id: AROAX2FJ77DC2JM67OZSY
  - test_output_variable: ${aws.kms.key:credstash_key-search:key_id}
  - path: /
  - max_session_duration: 3600
  - tags: {{ params.get("local_tags") }}
  - assume_role_policy_document: {"Statement": [{"Action": "sts:AssumeRole", "Effect":
      "Allow", "Principal": {"Service": "xyz.amazonaws.com"}}], "Version": "2012-10-17"}


aws_iam_role_policy_attachment.cluster-AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy


aws_iam_role_policy_attachment.cluster-AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.present:
  - role_name: ${aws.iam.role:aws_iam_role.cluster:resource_id}
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy
