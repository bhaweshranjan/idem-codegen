resource "aws_eks_cluster" "cluster" {
  name     = var.clusterName
  role_arn = aws_iam_role.cluster.arn
  version  = var.clusterVersion
  tags     = local.tags

  vpc_config {
    security_group_ids = [aws_security_group.cluster.id]
    subnet_ids         = concat(aws_subnet.cluster.*.id, aws_subnet.xyz_public_subnet.*.id)
  }
  enabled_cluster_log_types = ["controllerManager", "scheduler", "authenticator", "audit", "api"]
  lifecycle {
    ignore_changes = [vpc_config.0.subnet_ids]
  }
}

output "aws_eks_cluster-cluster-arn" {
  value = aws_eks_cluster.cluster.arn
}

output "aws_eks_cluster-cluster-endpoint" {
  value = aws_eks_cluster.cluster.endpoint
}

output "aws_eks_cluster-cluster-certificate_authority" {
  value = aws_eks_cluster.cluster.certificate_authority[0].data
}

output "aws_eks_cluster-identity-oidc-issuer" {
  value = aws_eks_cluster.cluster.identity[0].oidc[0].issuer
}
